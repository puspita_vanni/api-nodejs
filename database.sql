CREATE DATABASE node_api;

--\c into node_api

CREATE TABLE todo(
    todo_id serial PRIMARY KEY,
    description VARCHAR(255)
);